# Librerie Python


[pyaudio](https://people.csail.mit.edu/hubert/pyaudio/)

real-time IO usando portaudio


[librosa](https://librosa.org/doc/latest)

principalmente usata per l'analisi, in combutta con numpy

fa BPM detection, fornisce facilmente spettrogrammi

permette time-stretch e split/trim basati sui momenti di silenzio

non-realtime


[pydub](http://pydub.com/)

API per manipolare facilmente audio, non-realtime

permette di caricare e fare slicing, applicare alcuni effetti e fade


[scamp](http://scamp.marcevanstein.com/scamp.html)

Una Suite per Computer-Assisted Music in Python

bellissima libreria incentrata sulla composizione
