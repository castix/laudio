## prima di addentrarci

vediamo alcuni tool utili o diletevoli


[sox](https://sourceforge.net/projects/sox/) - SOund eXchange

è un coltellino svizzero per la manipolazione di stream audio!



[minimodem](http://www.whence.com/minimodem/)

utilizza ovviamente sox, modula o demodula una frase nel suono di un vero modem!


[libsndfile](http://libsndfile.github.io/libsndfile/)

indispensabile per leggere e scrivere file audio in vari formati

ad implementarli tutti usciresti pazzo


[lame](https://lame.sourceforge.io/)

mp3 encoder/decoder


#### utili per le web-radio

icecast

ices

liquidsoap

