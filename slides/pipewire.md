# PipeWire

anche video

anche container



## SPA - Simple Plugin API

- simple?

- plugin?

è utilizzata anche dal daemon e dalla libreria di pipewire, è il vero pezzo centrale

le applicazioni pensate per funzionare con pipewire utilizzano SPA


## Sessioni

Le sessioni in pipewire sono un altro concetto rispetto alle sessioni ladish.

si riferiscono alle policy usate per collegare le porte

senza un session manager, pipewire non puó fare nulla.

WirePlumber è il session manager di riferimento

nulla ti vieta di usare ladish con pipewire, ma qpwgraph ti permette di salvare le sessioni


## Patchbay

teoricamente puoi usare catia ed ogni patchbay per jack.

ma FalkTX non fornisce supporto per pipewire in quanto maintainer di Jack1 e Jack2.

helvum è una patchbay specifica per pipewire, GTK, mi sembra incompleta.

qpwgraph è un altra patchbay specifica, QT, mi sembra ottima!
