## Jack Audio Connection Kit

Indispensabile per avere controllo sul routing dell'audio. realtime

#### varie implementazioni
- Jack 1 - C
- Jack 2 - C++
- pipewire-jack


#### hai detto [network](https://jackaudio.org/faq/netjack.html)?

NO, se vuoi usare jack in una rete ti serve che entrambe le parti
comunichino con una tra queste implementazioni incompatibili tra loro

- netjack1
- netjack2
- jack.trip


#### vari launcher gui

+ qjackctl
+ cadence


#### bridge JACK <-> pulseaudio

prima non si poteva.

cadence ti permette di fare ció.

pipewire ti permette altrettanto
