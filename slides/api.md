#### API per scrivere applicazioni

- Di nuovo OSS

- Di nuovo ALSA

- [JACK](https://jackaudio.org) - Jack Audio Connection Kit

- [PulseAudio](https://www.freedesktop.org/wiki/Software/PulseAudio/) - Chi ha parlato di systemd?

- [PipeWire](https://docs.pipewire.org) - RedHat: che casotto, riscriviamo tutto

- [PortAudio](http://portaudio.com/) - Per fare le cose cross-platform

