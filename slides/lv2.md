# LV2


## rdf

[RDF](https://www.w3schools.com/XML/xml_rdf.asp) è uno standard del w3c per definire risorse in xml

## Plugin Discovery

I plugin host non hanno bisogno di caricare i plugin presenti sul sistema, leggono invece i "turtle" file

[serd](http://drobilla.net/software/serd) è un software usato per leggerli e gerarli

un binding cython è stato scritto dallo stesso autore, attualmente non mi compila :)


## Plugin host

- [carla](https://kx.studio/Applications:Carla)

- [jalv](http://drobilla.net/software/jalv/) - segmentation fault (core dumped)  jalv


## UI

Suil è un progetto nato per wrappare le interfacce utente dei plugin lv2, in modo che gli host non "crashino" quando
provi ad avviare un plugin scriito con qt dentro un host gtk, per fare un esempio


## [creare un plugin LV2](https://lv2plug.in/book/)

**run** è la funzione principale, nel thread che gestisce l'audio, quindi non puoi fare niente di blocking o allocare altra memoria. se hai bisogno di altri thread puoi avviarli in **activate**. run viene chiamata in chunk di N frame
