## ALSA

ma che è?

una collezione di driver ed utility.

la maggior parte driver sono inclusi con il kernel

altri sono disponibili e caricabili con dkms o modprobe

alcuni sono disponibili tramite ffado

Anche usando pipewire o jack, i driver rimangono quelli di alsa


[qui](https://github.com/tiwai/alsa-lib/blob/master/test/pcm.c) un esempio di come mandare tramite alsa pcm un
onda sinusoidale, in ogni modo possibile (e sono tanti!)


#### Sequencer ALSA

significa MIDI.
