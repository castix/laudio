#### Routing Audio / MIDI

Questa è la funzionalità principale che stavamo cercando quindi:
collegare applicazioni e plugin, ingressi ed uscite.

- Patchage
- Catia


In realtà basterebbe usare **jack_connect** conoscendo il nome delle porte


### Sessioni


##### LADISH
ti permette di salvare le connessioni tra le porte jack.
e di riaprire quindi i relativi programmi/plugin

Claudia è sempre Catia, ma con il supporto a ladish,
ma ha probabilmente bisogno di alcuni hack per
adattarsi al tuo use-case particolare (in Claudia-Launcher)

puoi usare ladish anche con gladish, o tramite cadence


##### [NON](https://non.tuxfamily.org/wiki/About) NSM

controllabile via OSC.

devi aggiungere un client **jackpatch** per salvare le connessioni fatte su jack

ovviamente incompatibile con ladish

[Manuale](http://non.tuxfamily.org/session-manager/doc/MANUAL.html)
